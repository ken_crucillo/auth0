var express = require('express');
var passport = require('passport');
var router = express.Router();
var request = require('request');

var env = {
  AUTH0_CLIENT_ID: process.env.AUTH0_CLIENT_ID,
  AUTH0_DOMAIN: process.env.AUTH0_DOMAIN,
  AUTH0_CALLBACK_URL: process.env.AUTH0_CALLBACK_URL || 'http://localhost:6006/callback'
};

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express', env: env });
});

router.get('/login',
  function(req, res){
    if(req.query["original-url"]!== undefined){
      req.session.apic = {
        original_url : req.query['original-url'],
        app_name : req.query['app-name']
      };
    }
    res.render('login', { env: env });
  });

router.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

router.get('/authenticate', function(req, res) {
  var str = req.headers['x-uri-in'];
  console.log('x-uri-in', str);
  var redirect_uri = str.substring(str.indexOf("redirect_uri")).replace("redirect_uri=","");
  console.log('redirect_uri', redirect_uri);
  var params = getQueryParams(redirect_uri);
  console.log('redirect_uri params', params);
  console.log('redirect_uri params', params.confirmation);
  var data = JSON.stringify(params);
  console.log(data);
  res.setHeader('API-Authenticated-Credential', "cn=spoon,o=ibm,c=USA");
  res.setHeader('API-OAUTH-METADATA-FOR-ACCESSTOKEN', `{"test":"${params.confirmation}"`);
  res.setHeader('API-OAUTH-METADATA-FOR-PAYLOAD', `{"test":"${params.confirmation}"`);
  res.send();
  // if(req.headers["x-ibm-authentication-url"]) res.json({ibm: req.headers["x-ibm-authentication-url"]});
  // else res.json(req.headers);
});   

function getQueryParams(qs) {
  qs = qs.split('+').join(' ');

  var params = {},
      tokens,
      re = /[?&]?([^=]+)=([^&]*)/g;

  while (tokens = re.exec(qs)) {
      params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
  }

  return params;
}

router.get('/callback',
  passport.authenticate('auth0', { failureRedirect: '/url-if-something-fails' }),
  function(req, res) {
    res.redirect(req.session.returnTo || '/user');
  });



module.exports = router;
