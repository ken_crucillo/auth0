var express = require('express');
var passport = require('passport');
var ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
var router = express.Router();
var request = require('request');

/* GET user profile. */
router.get('/', ensureLoggedIn, function(req, res, next) {
  if(req.session.apic !== undefined){
      console.log('APICONNECT',req.user);
      res.writeHead(302,
        { Location: [req.session.apic.original_url,"username=" + req.user._json.email,"confirmation=" + req.user.accessToken,"app-name=" + req.session.apic.app_name].join('&') }
      );
      res.end();
  }else{
      console.log('Normal',req.user);
      res.render('user', { user: req.user }); 
  }  
});

router.post('/info', function(req, res){
    console.log('query',req.query);
    console.log('body',req.body);
    if(req.body.bearer === undefined){
      res.status(401);
      res.end();
    }else{
      var options = {
        url: 'https://' + process.env.AUTH0_DOMAIN + '/userinfo',
        headers: {
          'Authorization': 'Bearer ' + req.body.bearer
        }
      };
      
      function callback(error, response, body) {
        if (!error && response.statusCode == 200) {
          var info = JSON.parse(body);
          res.json(info);
        }else{
          es.status(500);
          res.end();
        }
      }
      
      request(options, callback);
    }
});


module.exports = router;
